// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealString.h"
#include "Block.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, EditInlineNew)
class UBlock : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
		int id = 0;

	UPROPERTY(EditAnywhere)
		FString name;

	UPROPERTY(EditAnywhere)
		UMaterialInterface* material;

	UBlock();
};
