// Fill out your copyright notice in the Description page of Project Settings.

#include "Construction.h"
#include "MyProjectGameMode.h"
#include "Block.h"

// Sets default values
AConstruction::AConstruction()
{
	constructionMesh = CreateDefaultSubobject<UConstructionMesh>(TEXT("ConstructionMesh"));
	SetActorEnableCollision(true);

	constructionMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
}

// Called when the game starts or when spawned
void AConstruction::BeginPlay()
{
	Super::BeginPlay();

	constructionMesh->SetBlock(FIntVector(0, 0, 0), GetGameState()->blockDatabase[0]);
	constructionMesh->RegenerateMesh();

}
