// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ConstructionMesh.h"
#include "GameFramework/Actor.h"
#include "SGStateBase.h"
#include "Construction.generated.h"

UCLASS()
class MYPROJECT_API AConstruction : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AConstruction();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY(VisibleAnywhere)
		UConstructionMesh* constructionMesh;
	
	ASGStateBase* GetGameState() { return Cast<ASGStateBase>(GetWorld()->GetGameState()); }
};
