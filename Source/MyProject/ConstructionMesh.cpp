// Fill out your copyright notice in the Description page of Project Settings.

#include "ConstructionMesh.h"
#include "PhysicsEngine/BodySetup.h"
#include "PackedNormal.h"

UConstructionMesh::UConstructionMesh() {
	SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
}

const FConstructionMeshBlock* UConstructionMesh::GetBlockFromCollisionFaceIndex(int32 FaceIndex)
{
	int32 result = 0;

	if (FaceIndex >= 0) {
	
		return &blocks[faceToBlock[FaceIndex]];
	}

	return nullptr;
}

const FConstructionMeshBlock* UConstructionMesh::GetBlock(const FIntVector& pos) const {
	for (const auto& block : blocks) {
		if (block.pos == pos) {
			return &block;
		}
	}

	return nullptr;
}

void UConstructionMesh::DeleteBlock(const FIntVector& pos) {
	const auto* existingBlock = GetBlock(pos);

	if (existingBlock == nullptr) {
		return;
	}

	blocks.RemoveAll([&](const FConstructionMeshBlock& bl) { return bl.pos == pos; });
}

void UConstructionMesh::SetBlock(const FIntVector&  pos, const UBlock* block) {
	if (GetBlock(pos)) {
		return;
	}

	FConstructionMeshBlock bl;

	bl.pos = pos;
	bl.block = block;

	blocks.Push(bl);
}

void UConstructionMesh::RegenerateMesh() {
	TMap<int, FBlockSection> sections;
	TMap<int, TArray<int>> faceIdToBlockMap;

	int blockIdx = 0;
	faceToBlock.Empty();
	ClearAllMeshSections();

	// Go through all the blocks, and add them to their corresponding mesh section (determined by block id)
	for (const auto& block : blocks) {
		int id = block.block->id;

		if (sections.Find(id) == nullptr) {
			FBlockSection bs;
			bs.block = block.block;

			sections.Add(id, bs);
			faceIdToBlockMap.Add(id, TArray<int>());
		}

		auto* bs = sections.Find(id);

		CreateBlockMesh(bs, FVector(fBlockSize, fBlockSize, fBlockSize), FVector(block.pos)* fBlockSize, blockIdx);

		auto& arr = faceIdToBlockMap[id];

		for (int i = 0; i < 12; ++i) {
			arr.Push(blockIdx);
		}

		blockIdx++;
	}


	for (const auto& bs : sections) {
		const auto& sec = bs.Value;

		SetMaterial(bs.Key, bs.Value.block->material);
		CreateMeshSection(bs.Key, sec.vertices, sec.triangles, sec.normals, sec.UV0, sec.UV1, sec.UV2, sec.UV3, sec.colors, sec.tangents, true);

		for (const auto& idx : faceIdToBlockMap[bs.Key]) {
			faceToBlock.Push(idx);
		}
	}
}

void UConstructionMesh::CreateBlockMesh(FBlockSection* bs, FVector size, FVector pos, int blockIdx) {
	// Calculate a half offset so we get correct center of object
	float OffsetX = size.X / 2.0f;
	float OffsetY = size.Y / 2.0f;
	float OffsetZ = size.Z / 2.0f;

	// Define the 8 corners of the cube
	FVector p0 = FVector(OffsetX, OffsetY, -OffsetZ) + pos;
	FVector p1 = FVector(OffsetX, -OffsetY, -OffsetZ) + pos;
	FVector p2 = FVector(OffsetX, -OffsetY, OffsetZ) + pos;
	FVector p3 = FVector(OffsetX, OffsetY, OffsetZ) + pos;
	FVector p4 = FVector(-OffsetX, OffsetY, -OffsetZ) + pos;
	FVector p5 = FVector(-OffsetX, -OffsetY, -OffsetZ) + pos;
	FVector p6 = FVector(-OffsetX, -OffsetY, OffsetZ) + pos;
	FVector p7 = FVector(-OffsetX, OffsetY, OffsetZ) + pos;

	// Now we create 6x faces, 4 vertices each
	FVector Normal;
	FProcMeshTangent Tangent;

	// Front (+X) face: 0-1-2-3
	Normal = FVector(1, 0, 0);
	Tangent = FProcMeshTangent(0, 1, 0);
	BuildQuad(bs, p0, p1, p2, p3, Normal, Tangent, blockIdx);

	// Back (-X) face: 5-4-7-6
	Normal = FVector(-1, 0, 0);
	Tangent = FProcMeshTangent(0, -1, 0);
	BuildQuad(bs, p5, p4, p7, p6, Normal, Tangent, blockIdx);

	// Left (-Y) face: 1-5-6-2
	Normal = FVector(0, -1, 0);
	Tangent = FProcMeshTangent(1, 0, 0);
	BuildQuad(bs, p1, p5, p6, p2, Normal, Tangent, blockIdx);

	// Right (+Y) face: 4-0-3-7
	Normal = FVector(0, 1, 0);
	Tangent = FProcMeshTangent(-1, 0, 0);
	BuildQuad(bs, p4, p0, p3, p7, Normal, Tangent, blockIdx);

	// Top (+Z) face: 6-7-3-2
	Normal = FVector(0, 0, 1);
	Tangent = FProcMeshTangent(0, 1, 0);
	BuildQuad(bs, p6, p7, p3, p2, Normal, Tangent, blockIdx);

	// Bottom (-Z) face: 1-0-4-5
	Normal = FVector(0, 0, -1);
	Tangent = FProcMeshTangent(0, -1, 0);
	BuildQuad(bs, p1, p0, p4, p5, Normal, Tangent, blockIdx);
}

void UConstructionMesh::BuildQuad(FBlockSection* bs, FVector BottomLeft, FVector BottomRight, FVector TopRight, FVector TopLeft, FVector Normal, FProcMeshTangent Tangent, int blockIdx)
{
	int VertexOffset = bs->vertices.Num();

	int32 Index1 = VertexOffset++;
	int32 Index2 = VertexOffset++;
	int32 Index3 = VertexOffset++;
	int32 Index4 = VertexOffset++;

	bs->vertices.Push(BottomLeft);
	bs->UV0.Push(FVector2D(0.0f, 1.0f));
	bs->UV1.Push(FVector2D(0.0f, 0.0f));
	bs->UV2.Push(FVector2D(0.0f, 0.0f));
	bs->UV3.Push(FVector2D(0.0f, 0.0f));

	bs->vertices.Push(BottomRight);
	bs->UV0.Push(FVector2D(1.0f, 1.0f));
	bs->UV1.Push(FVector2D(0.0f, 0.0f));
	bs->UV2.Push(FVector2D(0.0f, 0.0f));
	bs->UV3.Push(FVector2D(0.0f, 0.0f));

	bs->vertices.Push(TopRight);
	bs->UV0.Push(FVector2D(1.0f, 0.0f));
	bs->UV1.Push(FVector2D(0.0f, 0.0f));
	bs->UV2.Push(FVector2D(0.0f, 0.0f));
	bs->UV3.Push(FVector2D(0.0f, 0.0f));

	bs->vertices.Push(TopLeft);
	bs->UV0.Push(FVector2D(0.0f, 0.0f));
	bs->UV1.Push(FVector2D(0.0f, 0.0f));
	bs->UV2.Push(FVector2D(0.0f, 0.0f));
	bs->UV3.Push(FVector2D(0.0f, 0.0f));

	bs->triangles.Push(Index1);
	bs->triangles.Push(Index2);
	bs->triangles.Push(Index3);
	bs->triangles.Push(Index1);
	bs->triangles.Push(Index3);
	bs->triangles.Push(Index4);


	// On a cube side, all the vertex normals face the same way
	bs->normals.Push(Normal);
	bs->normals.Push(Normal);
	bs->normals.Push(Normal);
	bs->normals.Push(Normal);

	bs->tangents.Push(Tangent);
	bs->tangents.Push(Tangent);
	bs->tangents.Push(Tangent);
	bs->tangents.Push(Tangent);

	bs->colors.Push(FColor::White);
	bs->colors.Push(FColor::White);
	bs->colors.Push(FColor::White);
	bs->colors.Push(FColor::White);
}
