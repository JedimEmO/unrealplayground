// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "ProceduralMeshComponent.h"
#include "ConstructionMesh.generated.h"

struct FConstructionMeshBlock {
	const UBlock* block; // Reference to the block description
	FIntVector pos;
};

struct FBlockSection {
	const UBlock* block;
	TArray<FVector> vertices;
	TArray<int32> triangles;
	TArray<FVector> normals;
	TArray<FColor> colors;
	TArray<FVector2D> UV0;
	TArray<FVector2D> UV1;
	TArray<FVector2D> UV2;
	TArray<FVector2D> UV3;
	TArray<FProcMeshTangent> tangents;
};

/**
 * 
 */
UCLASS()
class MYPROJECT_API UConstructionMesh : public UProceduralMeshComponent
{
	GENERATED_BODY()
		UConstructionMesh();
public:

	float fBlockSize = 50.0f;
	TArray<int32> faceToBlock;

	TArray<FConstructionMeshBlock> blocks;

	const FConstructionMeshBlock* GetBlock(const FIntVector& pos) const;
	const FConstructionMeshBlock* GetBlockFromCollisionFaceIndex(int32 FaceIndex);

	void SetBlock(const FIntVector&  pos, const UBlock* block);
	void DeleteBlock(const FIntVector& pos);

	void RegenerateMesh();

	void CreateBlockMesh(FBlockSection* bs, FVector size, FVector pos, int blockIdx);
	void BuildQuad(FBlockSection* bs, FVector BottomLeft, FVector BottomRight, FVector TopRight, FVector TopLeft, FVector Normal, FProcMeshTangent Tangent, int blockIdx);
};
