// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MyProjectCharacter.h"
#include "MyProjectProjectile.h"
#include "MyProjectGameMode.h"
#include "MyProjectHUD.h"
#include "Block.h"
#include "Construction.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "EngineGlobals.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AMyProjectCharacter

AMyProjectCharacter::AMyProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
}

void AMyProjectCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	Mesh1P->SetHiddenInGame(false, true);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMyProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Toggle input mode
	PlayerInputComponent->BindAction("ToggleInputMode", IE_Pressed, this, &AMyProjectCharacter::ToggleInputMode);

	// Bind jump events

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Quickbar selection actions

	for (int i = 0; i < 10; i++) {
		FInputActionBinding QS(*FString::Printf(TEXT("SelectQuickslot%d"), i), IE_Released);
		QS.ActionDelegate.GetDelegateForManualSet().BindLambda([this, i]() {
			this->ActivateQuickslot(i);
		});

		InputComponent->AddActionBinding(QS);
	}

	// Bind fire event
	FInputActionBinding place("Activate", IE_Released);
	place.ActionDelegate.GetDelegateForManualSet().BindLambda([this]() {
		this->OnActivate(0);
	});

	InputComponent->AddActionBinding(place);

	FInputActionBinding del("Delete", IE_Released);
	del.ActionDelegate.GetDelegateForManualSet().BindLambda([this]() {
		this->OnActivate(1);
	});

	InputComponent->AddActionBinding(del);

	FInputActionBinding list("ListInventory", IE_Released);
	list.ActionDelegate.GetDelegateForManualSet().BindLambda([this]() {
		auto* stack = NewObject<UItemStack>(GetPlayerController());

		GetPlayerController()->Inventory->AddItemStack(stack);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Added item %d"), stack->stackSize));
	});

	InputComponent->AddActionBinding(list);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AMyProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMyProjectCharacter::MoveRight);


	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &AMyProjectCharacter::YawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMyProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AMyProjectCharacter::PitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMyProjectCharacter::LookUpAtRate);
}

void AMyProjectCharacter::ToggleInputMode() {
	GetPlayerState()->ToggleInputMode();

	auto* hud = Cast<AMyProjectHUD>(GetPlayerController()->GetHUD());

	hud->ShowPlayerInventory(!IsAcceptingInput());
	
	GetPlayerController()->bShowMouseCursor = !IsAcceptingInput();

	if (IsAcceptingInput()) {
		GetPlayerController()->SetInputMode(FInputModeGameOnly{});
	}
	else {
		GetPlayerController()->SetInputMode(FInputModeGameAndUI{});
	}
}

void AMyProjectCharacter::ActivateQuickslot(int pos) {
	if (!IsAcceptingInput()) {
		return;
	}

	GetPlayerController()->ActionBar->SetActiveQuickslot(pos);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Selected block %d"), pos));
}

void AMyProjectCharacter::OnActivate(int mode)
{
	if (!IsAcceptingInput()) {
		return;
	}

	UWorld* world = GetWorld();

	if (world != nullptr) {
	
		const FRotator SpawnRotation = GetControlRotation();
		const auto camLocation = FirstPersonCameraComponent->GetComponentLocation();

		// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
		const FVector start = camLocation;
		const FVector end = start + (SpawnRotation.Vector() * 300);

		FHitResult result;
		FCollisionQueryParams params;

		params.AddIgnoredActor(this);
		params.bReturnFaceIndex = true;

		bool hit = world->LineTraceSingleByChannel(result, start, end, ECC_Camera, params);

		static int spawnnum = 1;

		if (hit) {
			const auto* actor = result.Actor.Get();

			if (result.Component.Get()) {
				auto* cmp = result.Component.Get();
				auto* mesh = Cast<UConstructionMesh>(cmp);

				if (mesh) {
					const auto* b = mesh->GetBlockFromCollisionFaceIndex(result.FaceIndex);
				
					if (b != nullptr) {

						if (mode == 0) {
							auto newPos = b->pos + FIntVector(result.ImpactNormal);

							UBlock* block = GetGameState()->blockDatabase[currentBlock];

							mesh->SetBlock(newPos, block);
						}
						else if (mode == 1) {
							mesh->DeleteBlock(b->pos);
							GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Deleted block")));
						}
						
						mesh->RegenerateMesh();
					}
				}
			}
		}
	}
}

void AMyProjectCharacter::PitchInput(float Val)
{
	if (!IsAcceptingInput()) {
		return;
	}

	AddControllerPitchInput(Val);
}

void AMyProjectCharacter::YawInput(float Val)
{
	if (!IsAcceptingInput()) {
		return;
	}

	AddControllerYawInput(Val);
}


void AMyProjectCharacter::MoveForward(float Value)
{
	if (!IsAcceptingInput()) {
		return;
	}

	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AMyProjectCharacter::MoveRight(float Value)
{
	if (!IsAcceptingInput()) {
		return;
	}

	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AMyProjectCharacter::TurnAtRate(float Rate)
{
	if (!IsAcceptingInput()) {
		return;
	}

	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMyProjectCharacter::LookUpAtRate(float Rate)
{
	if (!IsAcceptingInput()) {
		return;
	}

	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AMyProjectCharacter::IsAcceptingInput() {
	return GetPlayerState()->playerInputMode == InputMode::CHARACTER;
}