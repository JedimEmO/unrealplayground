// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MyProjectGameMode.h"
#include "MyProjectHUD.h"
#include "MyProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyProjectGameMode::AMyProjectGameMode()
	: Super()
{
	DefaultPawnClass = AMyProjectCharacter::StaticClass();

	// use our custom HUD class
	HUDClass = AMyProjectHUD::StaticClass();
}
