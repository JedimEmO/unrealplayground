// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MyProjectHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "Engine.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "SGPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

AMyProjectHUD::AMyProjectHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
}


void AMyProjectHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

ASGPlayerController* AMyProjectHUD::GetPlayerController() {
	auto* world = GetWorld();

	if (world && GEngine) {
		auto* ctrl = Cast<ASGPlayerController>(world->GetFirstPlayerController());

		return ctrl;
	}

	return nullptr;
}

void AMyProjectHUD::ShowCraftingGUI()
{
	auto* ctrl = GetPlayerController();

	if (!ctrl) {
		return;
	}

	ShowPlayerInventory(false);

	UserInterface = CreateWidget<UUserWidget>(ctrl, ctrl->CraftingInterfaceClass);
	UserInterface->AddToViewport();
}

void AMyProjectHUD::ShowPlayerInventory(bool state)
{
	auto* world = GetWorld();

	if (world && GEngine) {
		auto* ctrl = Cast<ASGPlayerController>(world->GetFirstPlayerController());

		if (ctrl) {
			if (!UserInterface && state) {
				UserInterface = CreateWidget<UUserWidget>(ctrl, ctrl->UserInterfaceClass);
			}
		}
	}

	if (UserInterface) {
		if (state) {
			UserInterface->AddToViewport();
		}
		else {
			UserInterface->RemoveFromViewport();
			UserInterface = nullptr;
		}
	}
}

void AMyProjectHUD::BeginPlay()
{
	auto* world = GetWorld();

	if (world && GEngine) {
		auto* ctrl = Cast<ASGPlayerController>(world->GetFirstPlayerController());

		if (ctrl) {
			if (!HUD && ctrl->HudClass) {
				HUD = CreateWidget<UUserWidget>(ctrl, ctrl->HudClass);

				if (UserInterface) {
					UserInterface->RemoveFromViewport();
				}

				HUD->AddToViewport();
			}
		}
	}
}
