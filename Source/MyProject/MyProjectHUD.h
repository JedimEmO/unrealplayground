// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SGPlayerController.h"
#include "MyProjectHUD.generated.h"

class SInventoryContainerWidget;

UCLASS()
class AMyProjectHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMyProjectHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;


	void BeginPlay();

	UFUNCTION(BlueprintCallable)
		void ShowPlayerInventory(bool state);

	UFUNCTION(BlueprintCallable)
		void ShowCraftingGUI();

	ASGPlayerController* GetPlayerController();
private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	UUserWidget* UserInterface = nullptr;
	UUserWidget* HUD = nullptr;
};

