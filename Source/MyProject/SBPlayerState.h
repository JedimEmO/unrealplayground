// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "items/itemContainer.h"
#include "items/itemStack.h"
#include "SBPlayerState.generated.h"

enum class InputMode {
	CHARACTER,
	UI
};

/**
 * 
 */
UCLASS()
class MYPROJECT_API ASBPlayerState : public APlayerState
{
	GENERATED_BODY()

		ASBPlayerState();

public:

	InputMode playerInputMode = InputMode::CHARACTER;

	void ToggleInputMode() {
		if (playerInputMode == InputMode::CHARACTER) {
			playerInputMode = InputMode::UI;
		}
		else {
			playerInputMode = InputMode::CHARACTER;
		}
	}
};
