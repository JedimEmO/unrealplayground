// Fill out your copyright notice in the Description page of Project Settings.

#include "SGPlayerController.h"
#include "MyProjectHUD.h"


void ASGPlayerController::PickUpItemStack(UItemContainer* ContainerFrom, UItemStack* stack)
{
	if (CursorItem != nullptr || stack == nullptr) {
		return;
	}

	CursorItem = stack;
	CursorContainer = ContainerFrom;
}

void ASGPlayerController::DropCursorItemStack(UItemContainer* ContainerTo)
{
	if (CursorItem == nullptr || ContainerTo == nullptr) {
		return;
	}

	ContainerTo->AddItemStack(CursorItem);

	CursorContainer->RemoveItemStack(CursorItem);
	CursorItem = nullptr;
}

void ASGPlayerController::BeginPlay()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Creating player inventory"));

	// Set up the inventory
	Inventory = NewObject<UItemContainer>(this);

	Inventory->bCanExpand = true;
	Inventory->SetSize(20);

	ActionBar = NewObject<UActionBar>(this);

	for (int i = 0; i < 10; i++) {
		ActionBar->AddQuickslot();
	}
}

ASGPlayerController::ASGPlayerController() {
}

void ASGPlayerController::BeginCraftingSession(UCraftingBlueprint* Blueprint)
{
	auto* hud = Cast<AMyProjectHUD>(GetHUD());

	if (!hud) {
		return;
	}

	CraftingSession = NewObject<UCraftingSession>(this);

	CraftingSession->Blueprint = Blueprint;
	hud->ShowCraftingGUI();
}