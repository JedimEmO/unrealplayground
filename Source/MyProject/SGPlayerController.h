// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "items/itemContainer.h"
#include "items/ActionBar.h"
#include "items/CraftingBlueprint.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/UserWidget.h"
#include "SGPlayerController.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class MYPROJECT_API ASGPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASGPlayerController();

	void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCraftingSession* CraftingSession;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UCraftingBlueprint*> PlayerBlueprints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItemContainer* Inventory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UActionBar* ActionBar;

	UFUNCTION(BlueprintCallable)
		void PickUpItemStack(UItemContainer* Container, UItemStack* stack);

	UFUNCTION(BlueprintCallable)
		void DropCursorItemStack(UItemContainer* Container);
	
	UFUNCTION(BlueprintCallable)
		void BeginCraftingSession(UCraftingBlueprint* Blueprint);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UItemStack* CursorItem; // The current item stack held by the mouse cursor (Used for moving items)

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UItemContainer* CursorContainer; // The current item stack held by the mouse cursor (Used for moving items)

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UUserWidget> UserInterfaceClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UUserWidget> CraftingInterfaceClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UUserWidget> HudClass;
};
