// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Block.h"	
#include "items/item.h"
#include "GameFramework/GameStateBase.h"
#include "UiAssets.h"
#include "SGStateBase.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API ASGStateBase : public AGameStateBase
{
	GENERATED_BODY()
public:
	ASGStateBase();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UBlock*> blockDatabase; // The blocks available in the game

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItemDatabase* ItemDatabase; // All the base items in the game

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UUiAssets* UiAssets; // All UI assets
};
