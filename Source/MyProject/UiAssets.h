// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Slate/SlateBrushAsset.h"
#include "UiAssets.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class MYPROJECT_API UUiAssets : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere)
		FSlateBrush ItemBackdrop;

	UPROPERTY(EditAnywhere)
		FSlateBrush PanelBackdrop;
};
