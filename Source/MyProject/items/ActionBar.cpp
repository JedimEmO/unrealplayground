// Fill out your copyright notice in the Description page of Project Settings.

#include "ActionBar.h"




UActionBar::UActionBar()
{
}

void UActionBar::AddQuickslot()
{
	auto slot = NewObject<UQuickslot>(this);
	QuickSlots.Push(slot);
}

void UActionBar::BindQuickslot(int idx, UItemStack* ItemStack)
{
	QuickSlots[idx]->BindItemStack(ItemStack);
}