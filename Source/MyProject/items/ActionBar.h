// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Quickslot.h"
#include "ActionBar.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class MYPROJECT_API UActionBar : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UQuickslot*> QuickSlots;

	UFUNCTION(BlueprintCallable)
	int ActiveQuickslot() {
		return _ActiveQuickslot;
	}

	UFUNCTION(BlueprintCallable)
	void SetActiveQuickslot(int slot) {
		if (slot < 0 || slot > 9) {
			return;
		}

		_ActiveQuickslot = slot;
	}

	UFUNCTION(BlueprintCallable)
	void BindQuickslot(int idx, UItemStack* ItemStack);

	UActionBar();

	void AddQuickslot();

private:
	int _ActiveQuickslot = 0;

};
