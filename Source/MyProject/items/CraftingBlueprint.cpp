// Fill out your copyright notice in the Description page of Project Settings.

#include "CraftingBlueprint.h"

FString UItemDescription::GetItemDescriptionText()
{
	FString ret;

	ret = FString::Printf(TEXT("%s\n\n"), *name);
	ret += FString::Printf(TEXT("%s\n\n"), *FString::Join(Classification->MaterialClasses, TEXT(",")));

	for (const auto& prop : Classification->Properties) {
		ret += FString::Printf(TEXT("%s: %.1f\n"), *prop.Key, prop.Value);
	}

	return ret;
}

float UItemBlueprintSlot::ComputeSuitability(const TMap<FString, float>& properties)
{
	float Suitability = 0.0f;

	for (const auto& Weight : PropertyWeights) {
		float PropertyContribution = 0.0f;

		for (const auto Prop : properties) {
			if (Prop.Key == Weight.Key) {
				PropertyContribution = Prop.Value * Weight.Value;
				break;
			}
		}

		Suitability += PropertyContribution;
	}

	return Suitability;
}

void UCraftingSession::AddItemToSlot(const UItemBlueprintSlot* slot, UItemContainer* SourceContainer, UItemStack* SourceStack)
{
	if (!slot || !SourceContainer || !SourceStack) {
		return;
	}

	if (SourceStack->stackSize < slot->RequiredQuantity) {
		return;
	}

	auto existing = SlotContent.Find(slot->SlotID);

	if (existing) {
		SourceContainer->AddItemStack(*existing);
		SlotContent.Remove(slot->SlotID);
	}

	UItemStack* StackToAdd = NewObject<UItemStack>();

	StackToAdd->item = SourceStack->item;
	SourceStack->stackSize -= slot->RequiredQuantity;

	if (SourceStack->stackSize == 0) {
		SourceContainer->RemoveItemStack(SourceStack);
	}

	SlotContent.Add(slot->SlotID, StackToAdd);
	SessionChanged.Broadcast();
}

FString UCraftingSession::CalculateOutputPropetiesText()
{
	auto props = CalculateOutputPropeties();

	FString ret;

	for (const auto& prop : props) {
		ret += FString::Printf(TEXT("%s: %.1f\n"), *prop.Key, prop.Value);
	}

	return ret;
}

TMap<FString, float> UCraftingSession::CalculateOutputPropeties()
{
	TMap<FString, float> ret;

	if (!this->Blueprint) {
		return ret;
	}
	TMap<FString, float> SuitabilityMap;

	for (const auto& InSlot : Blueprint->MaterialSlots) {
		UItemStack** InMaterial = SlotContent.Find(InSlot.Key);

		if (InMaterial && (*InMaterial)->item) {
			SuitabilityMap.Add(InSlot.Key, InSlot.Value->ComputeSuitability((*InMaterial)->item->Classification->Properties));
		}
		else {
			SuitabilityMap.Add(InSlot.Key, 0.0f);
		}
	}

	for (const auto& OutSlot : this->Blueprint->OutputSlots) {
		float Max = OutSlot.Value->MaxOutputValue;
		float total = 0.0f;

		for (const auto& Effect : OutSlot.Value->SlotSuitabilityEffect) {
			if (SuitabilityMap.Find(Effect.Key)) {
				total += Effect.Value * SuitabilityMap[Effect.Key];
			}
		}

		ret.Add(OutSlot.Key) = total;
	}

	return ret;
}

void UCraftingSession::Finalize(UItemContainer* TargetInventory)
{
	if (!Blueprint) {
		return;
	}

	UItemDescription* Made = NewObject<UItemDescription>(TargetInventory);

	Made->icon = Blueprint->OutputItem->icon;
	Made->itemId = Blueprint->OutputItem->itemId;
	Made->name = Blueprint->OutputItem->name;
	Made->Classification = NewObject<UCraftingMaterialClassification>(TargetInventory);
	Made->Classification->Properties = CalculateOutputPropeties();
	Made->Classification->MaterialClasses = Blueprint->OutputItem->Classification->MaterialClasses;

	UItemStack* OutStack = NewObject<UItemStack>(TargetInventory);
	OutStack->item = Made;
	OutStack->stackSize = 1;

	TargetInventory->AddItemStack(OutStack);
}
