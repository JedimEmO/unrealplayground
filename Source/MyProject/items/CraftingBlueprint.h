// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "itemStack.h"
#include "itemContainer.h"
#include "UObject/NoExportTypes.h"
#include "CraftingBlueprint.generated.h"

/**
* Specifies all the material classes this material falls into. This dictates what properties it has
*/
UCLASS(BlueprintType)
class UCraftingMaterialClassification : public UDataAsset{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSet<FString> MaterialClasses; // All the classifications this material falls into

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, float> Properties; // All property values for this classification
};

UCLASS(BlueprintType)
class UItemBlueprintSlot : public UDataAsset {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString SlotID; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString RequiredClass; // String that must be present in the items material class specification set
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int RequiredQuantity = 1; // The amount of a material required

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Optional = false; // If set to false, this slot does not need to be filled to complete a craft

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, float> PropertyWeights; // Used to compute how good a selected material is for this blueprint slot

	UFUNCTION(BlueprintCallable)
		float ComputeSuitability(const TMap<FString, float>& properties);
};


UCLASS(BlueprintType)
class MYPROJECT_API UBlueprintOutputProperty : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, float> SlotSuitabilityEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString OutputPropertyName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MaxOutputValue; // This is The maximum output value, given 100% suitability in all dependent slots and properties
};

/**
 * 
 */
UCLASS(BlueprintType)
class MYPROJECT_API UCraftingBlueprint : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString BlueprintName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItemDescription* OutputItem; // Base output item, its properties will be recalculated based on input to the blueprint

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, UBlueprintOutputProperty*> OutputSlots; // Generated output properties for this blueprint

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, UItemBlueprintSlot*> MaterialSlots; // All the input slots for materials in this blueprint
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCraftingSessionChanged);

UCLASS(BlueprintType)
class MYPROJECT_API UCraftingSession: public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCraftingBlueprint* Blueprint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, class UItemStack*> SlotContent; // Actual materials assigned to each slot in this craft

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CurrentCraftingStage = 0; // The current stage of crafting

	UFUNCTION(Blueprintcallable)
		void AddItemToSlot(const UItemBlueprintSlot* slot, UItemContainer* SourceContainer, UItemStack* SourceStack);

	UPROPERTY(EditAnywhere, BlueprintAssignable, Category = "Events")
		FCraftingSessionChanged SessionChanged;

	UFUNCTION(BlueprintCallable)
		TMap<FString, float> CalculateOutputPropeties();

	UFUNCTION(BlueprintCallable)
		FString CalculateOutputPropetiesText();

	UFUNCTION(BlueprintCallable)
		void Finalize(UItemContainer* TargetInventory);
};