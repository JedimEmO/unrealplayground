// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/DragDropOperation.h"
#include "itemContainer.h"
#include "ItemStackDragDropOperation.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API UItemStackDragDropOperation : public UDragDropOperation
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UItemContainer* SourceContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UItemStack* ItemStack;
};
