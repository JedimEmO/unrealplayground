// Fill out your copyright notice in the Description page of Project Settings.

#include "Quickslot.h"
#include "ActionBar.h"

void UQuickslot::Unbind()
{
	ReferencedItemStack = nullptr;
}

void UQuickslot::BindItemStack(UItemStack* Stack)
{
	ReferencedItemStack = Stack;
}

UQuickslot::UQuickslot()
{
}
