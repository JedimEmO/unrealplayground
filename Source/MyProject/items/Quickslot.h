// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "itemStack.h"
#include "Quickslot.generated.h"

class UActionBar;

/**
 * 
 */
UCLASS(BlueprintType)
class MYPROJECT_API UQuickslot : public UObject
{
	GENERATED_BODY()

public:
	UQuickslot();

	UActionBar* ActionBar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItemStack* ReferencedItemStack = nullptr;

	UFUNCTION(BlueprintCallable)
		void Unbind();

	UFUNCTION(BlueprintCallable)
		void BindItemStack(UItemStack* Stack);

private:
};

