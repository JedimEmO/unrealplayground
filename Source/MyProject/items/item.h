#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "Item.generated.h"


UCLASS(BlueprintType)
class UItemDescription : public UDataAsset {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UCraftingMaterialClassification* Classification;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString itemId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString name;

	UFUNCTION(BlueprintCallable)
		FString GetItemDescriptionText();
};

UCLASS(BlueprintType)
class UConsumableItem : public UItemDescription {
	GENERATED_BODY()
};

UCLASS(BlueprintType)
class UFoodItem : public UConsumableItem {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float fillValue = 1.0f;
};

/**
* Property that holds items.
*/
UCLASS(EditInlineNew, BlueprintType)
class MYPROJECT_API UItemDatabase : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UItemDescription*> items;
};