#include "itemContainer.h"

bool UItemContainer::AddItemStack(UItemStack* stack) {
	int idx = GetFirstFreeInventorySlotIndex(true);

	if (idx < 0) {
		return nullptr;
	}

	UInventorySlot* Slot = Slots[idx];

	if (Slot) {
		Slot->ItemStack = stack;

		Change();

		return true;
	}

	return false;
}

void UItemContainer::RemoveItemStack(const UItemStack* stack) {
	for (auto& slot : Slots) {
		if (slot->ItemStack == stack) {
			slot->ItemStack = nullptr;
			break;
		}
	}

	Change();
}

int UItemContainer::GetFirstFreeInventorySlotIndex(bool bAllowGrow) {
	int idx = 0;

	for (auto* slot : Slots) {
		if (!slot) {
			continue;
		}

		if (!slot->ItemStack) {
			return idx;
		}

		idx++;
	}

	if (bAllowGrow && bCanExpand) {
		SetSize(Slots.Num() + 1);
		Change();
		return idx;
	}

	return -1;
}

void UItemContainer::Change()
{
	InventoryChanged.Broadcast();
}

void UItemContainer::SetSize(int32 size) {
	int toAdd = size - Slots.Num();

	for (int slot = 0; slot < toAdd; ++slot) {
		auto* addedSlot = NewObject<UInventorySlot>();
		addedSlot->ParentContainer = this;
		Slots.Push(addedSlot);
	}
}

UItemContainer::UItemContainer()
{
}