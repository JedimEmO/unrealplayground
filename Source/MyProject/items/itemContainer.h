#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "itemStack.h"
#include "ItemContainer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInventoryChanged);

UCLASS(BlueprintType)
class UInventorySlot : public UObject {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UItemStack* ItemStack; // Can be null

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UItemContainer* ParentContainer; // Can be null

	UFUNCTION(BlueprintCallable)
		int GetStackSize() {
		if (ItemStack) {
			return ItemStack->stackSize;
		}

		return 0;
	}

	UInventorySlot() { ItemStack = nullptr; }
};

/**
* Property that holds items.
*/
UCLASS(BlueprintType)
class MYPROJECT_API UItemContainer : public UObject
{
	GENERATED_BODY()

public:
		UItemContainer();

public:
	UPROPERTY(EditAnywhere, BlueprintAssignable, Category = "Events")
	FInventoryChanged InventoryChanged;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanExpand = true; // Allow the inventory to add more slots as required

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UInventorySlot*> Slots;

	UFUNCTION()
	void SetSize(int32 size);

	UFUNCTION(BlueprintCallable, Category = "Items")
	bool AddItemStack(UItemStack* stack);

	UFUNCTION(BlueprintCallable, Category="Items")
	void RemoveItemStack(const UItemStack* stack);

	UFUNCTION(BlueprintCallable)
	int GetFirstFreeInventorySlotIndex(bool bAllowGrow = false);

	UFUNCTION(BlueprintCallable)
	void Change();
};