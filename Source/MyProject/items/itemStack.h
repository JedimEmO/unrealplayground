#pragma once

#include "item.h"
#include "Object.h"
#include "ItemStack.generated.h"

/**
* Property that holds items.
*/
UCLASS(BlueprintType)
class MYPROJECT_API UItemStack : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int stackSize = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UItemDescription* item;
};